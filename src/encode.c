#include "encode.h"

int compressionMode = 0;

char* readFileencode(char* fileName,size_t* length){
    FILE *f = fopen(fileName, "rb");
    fseek(f, 0, SEEK_END);
    *length = (size_t)ftell(f);
    fseek(f, 0, SEEK_SET);
    char *json_buffer = malloc(*length + 1);
    fread(json_buffer, *length, 1, f);
    json_buffer[*length] = '\0';
    fclose(f);
    return json_buffer;
}

cJSON *getDocument(char *contextURL, char *(*documentLoader)(char *url))
{
    char *context = documentLoader(contextURL);
    cJSON *contextDoc = cJSON_Parse(context);
    return cJSON_GetObjectItem(contextDoc,"@context");
}

struct encoderContext{
  char* parentId;
};
typedef struct encoderContext encoderContext;

bool compareString(char* c1,size_t c1len,char* c2){
  for (int i = 0; i < c1len; i++)
  {
    if(*(c1 + i) != *(c2+i)) return false;
  }
  return true;
}


typedef void (*cbor_load_callback_t)(cJSON *, const struct cbor_callbacks *, void *,encoderContext* encoderContext);

//This function converts cjson to cbor. Note that source points to cjson data. 
cbor_item_t *cjson_cbor_load(void *source,
                             cbor_load_callback_t cbor_load_callback) {
  static struct cbor_callbacks callbacks = {
      .uint8 = &cbor_builder_uint8_callback,
      .uint16 = &cbor_builder_uint16_callback,
      .uint32 = &cbor_builder_uint32_callback,
      .uint64 = &cbor_builder_uint64_callback,
      .negint8 = &cbor_builder_negint8_callback,
      .negint16 = &cbor_builder_negint16_callback,
      .negint32 = &cbor_builder_negint32_callback,
      .negint64 = &cbor_builder_negint64_callback,
      .string = &cbor_builder_string_callback,
      .string_start = &cbor_builder_string_start_callback,
      .byte_string=&cbor_builder_byte_string_callback,
      .byte_string_start = &cbor_builder_byte_string_start_callback,
      .array_start = &cbor_builder_array_start_callback,
      .indef_array_start = &cbor_builder_indef_array_start_callback,
      .map_start = &cbor_builder_map_start_callback,
      .indef_map_start = &cbor_builder_indef_map_start_callback,
      .indef_break = &cbor_builder_indef_break_callback,
      .null = &cbor_builder_null_callback,
      .boolean = &cbor_builder_boolean_callback,
      .float2 = &cbor_builder_float2_callback,
      .float4 = &cbor_builder_float4_callback,
      .float8 = &cbor_builder_float8_callback,
      .undefined=&cbor_builder_undefined_callback,
  };

  /* Context stack */
  struct _cbor_stack stack = _cbor_stack_init();

  /* Target for callbacks */
  struct _cbor_decoder_context context = (struct _cbor_decoder_context){
      .stack = &stack,
  };
  encoderContext encoderContext = {
    .parentId=""
  };
  cbor_load_callback(source, &callbacks, &context,&encoderContext);

  return context.root;
}

// XXX: This is not portable 
// TODO: Find a better way? Also doubles wont store all values for int64_t
void handle_int_values(double value, const struct cbor_callbacks *callbacks, void *context){
  if (value >= 0) {
    if(value<=INT8_MAX){
      callbacks->uint8(context, value);
    }else if(value<=INT16_MAX){
      callbacks->uint16(context, value);
    }else if(value<=INT32_MAX){
      callbacks->uint32(context, value);
    }else{
      callbacks->uint64(context, value);
    }
  }else {
    if(value>INT8_MIN){
      callbacks->negint8(context, value);
    }else if(value>INT16_MIN){
      callbacks->negint16(context, value);
    }else if(value>INT32_MIN){
      callbacks->negint32(context, value);
    }else{
      callbacks->negint64(context, value);
    }
  }
}

void handle_float_values(double value, const struct cbor_callbacks *callbacks, void *context){
  if(value <= FLT_MAX && value>=FLT_MIN){
    callbacks->float4(context,value);
  }else{
    callbacks->float8(context,value);
  }
}

void handle_string_values(unsigned char* value,const struct cbor_callbacks *callbacks, void* context){
  callbacks->string(context,value,strlen(value));
}

//This function is used to parse the cJSON stream and process the objects according to their types
//Its name could have been a cjson parser as well
void cjson_cbor_stream_decode(cJSON *source,
                              const struct cbor_callbacks *callbacks,
                              void *context,encoderContext* encoderContext) {
  //process according to type of cjson object
  switch (source->type) {
    case cJSON_False: {
      callbacks->boolean(context, false);
      return;
    }
    case cJSON_True: {
      callbacks->boolean(context, true);
      return;
    }
    case cJSON_NULL: {
      callbacks->null(context);
      return;
    }
    case cJSON_Number: {
      //Currently there is no good way to distinguish berween ints and doubles
      // TODO: Find a better way
      if (fabs(source->valuedouble - source->valueint) > DBL_EPSILON) {
        handle_float_values(source->valuedouble,callbacks,context);
      } else {
        handle_int_values(source->valuedouble,callbacks,context);
      }
      return;
    }
    case cJSON_String: {
      // XXX: Assume cJSON handled unicode correctly
      //TODO: Handle string types here
      handle_string_values(source->valuestring,callbacks,context);
      return;
    }
    case cJSON_Array: {
      callbacks->array_start(context, cJSON_GetArraySize(source));
      cJSON *item = source->child;
      while (item != NULL) {
        cjson_cbor_stream_decode(item, callbacks, context,encoderContext);
        item = item->next;
      }
      return;
    }
    //Objects can be recursive so we need to keep a track of the parents while recursing through objects within objects.
    //XX Note that cjson doesnt allow us to use integers as keys, whereas cbor can use as integers as keys. Thus, we convert it to an integer
    //XX using atoll. Thus, we assume that it will be an integer
    case cJSON_Object: {
      callbacks->map_start(context, cJSON_GetArraySize(source));
      cJSON *item = source->child;
      while (item != NULL) {
        char* key = (unsigned char *)item->string;
        encoderContext->parentId = key;
        if(compressionMode) {
          handle_int_values(atoll(encoderContext->parentId),callbacks,context);
        }else{
          callbacks->string(context, key, strlen(item->string));
        }
        cjson_cbor_stream_decode(item, callbacks, context,encoderContext);
        item = item->next;
      }
      return;
    }
  }
}

/**
 * @brief The function to encode to cbor using 2 modes: Uncompressed and Compressed
 *        as defined in the CBORLD spec.
 *
 * @param json the json data to be compressed
 * @param compressionMode 0 or 1 as the compression mode 
 * @param length the length of data
 * @param error a variable to manage errors
 */

char* encode_to_cbor_with_mode(cJSON* json,int compressionMode,size_t* length,void* (*panic)(char* error)){
   cbor_item_t *cbor = cjson_cbor_load(json, cjson_cbor_stream_decode);
    /* Print out CBOR bytes */
    unsigned char *buffer;
    size_t buffer_size,
        cbor_length = cbor_serialize_alloc(cbor, &buffer, &buffer_size);
    
    unsigned char *suffix = buffer;
    char buffer_prefix_tag = (char) 0xd9;
    char buffer_prefix_cborld = (char) 0x05;
    char buffer_prefix_compression = (char) compressionMode;
    char* finalBuffer = malloc (sizeof (char) * (cbor_length+3));
    *(finalBuffer) = buffer_prefix_tag;
    *(finalBuffer+1) = buffer_prefix_cborld;
    *(finalBuffer+2) = buffer_prefix_compression;
    for (int i = 0; i < cbor_length; i++)
    {
        *(finalBuffer+i+3) = suffix[i];
    }
    printf("\n %ld | %ld \n",cbor_length,buffer_size);
    *(length) = cbor_length+3;
    cJSON_Delete(json);
    cbor_decref(&cbor);
    return finalBuffer;
}

//There are 2 modes in the CBORLD specification. This is the mode uncompressed.
// encode_to_cbor
char* encode_to_cbor_uncompressed(char* json_buffer,size_t* length,void* (*panic)(char* error)) {
    /* Convert between JSON and CBOR */
    cJSON *json = cJSON_Parse(json_buffer);
    return encode_to_cbor_with_mode(json,0,length,panic);
}

//This is the 2nd mode called compressed mode as specified in the CBORLD library.
char* encode_to_cbor_compressed(char* json_buffer,size_t* length,void* (*panic)(char* error),char* (*documentLoader)(char* url),char* contextMap){
    compressionMode = 1;
    //Lets use the cJSON library to parse the char* to json format
    cJSON* json = cJSON_Parse(json_buffer);
    cJSON* ctxMap = cJSON_Parse(contextMap); //The input context map is the char* containing context in JSON format.
    //Now we obtain the transforMap using the context map
    cJSON* transformMap = compress(json,documentLoader,panic,ctxMap);
    //TODO: Debug to find link issues with child,prev,next . For now just printing and parsing 
    // again to be safe from errors . But can be slow
    char* tran = cJSON_Print(transformMap);
    //We may print it for debugging purposes.
    printf("%s",tran);
    //now we have the transformed json
    cJSON* tranjson = cJSON_Parse(tran);
    return encode_to_cbor_with_mode(tranjson,1,length,panic);
}
