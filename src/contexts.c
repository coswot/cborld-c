#include "contexts.h"

int add_context(char* contextUrl,char* contextDocument,ContextData* contextData){
    cJSON* contextJson = cJSON_Parse(contextDocument);
    if(contextJson != NULL){
        cJSON* context = cJSON_GetObjectItem(contextJson,"@context");
        if(context != NULL) {
            process_context(context,"",contextData);
        }
    }
}

int get_dataType(char* type,ContextData* context){
    if(strcmp(type,"@id")==0 || has_aliases_entry(context->aliases,"id",type)){
        return DATA_Uri;
    }else if(strcmp(type,"@type")==0 || has_aliases_entry(context->aliases,"type",type)){
        return DATA_Type;
    }else if(strcmp(type,"@vocab")==0){
        return DATA_VocabTerm;
    }else if(strcmp(type,"https://w3id.org/security#multibase")==0){
        return DATA_Multibase;
    }else if(strcmp(type,"http://www.w3.org/2001/XMLSchema#date")==0){
        return DATA_XsdDate;
    }else if( (strcmp(type,"http://www.w3.org/2001/XMLSchema#dateTime")==0) 
            ||(strcmp(type,"xsd:dateTime")==0)) {
        return DATA_XsdDateTime;
    }else return -1;
}
 /*
 
 {
    "a":{
        "@context":{
            "b":
        }
    }
 }
 ['a','a:b']

 */

void process_context(cJSON* context,char* termSuffix,ContextData* contextData){
    int size = cJSON_GetArraySize(context);
    char* keys[size];
    cJSON* ctx;
    int i = 0;
    size_t suffixLen = strlen(termSuffix);
    // load aliases and keys
    cJSON_ArrayForEach(ctx,context){
        // Satendra: BUG: +2 why we need extra 1 byte?
        // Kamal: I am adding 1 byte to add NULL character at the end. 
        char* key = malloc((strlen(ctx->string)+1)*sizeof(char));
	memset(key, '\0', (strlen(ctx->string)+1)*sizeof(char));
        if(get_termMap_id(contextData->termMap,ctx->string) != -1) {
            size--;
            continue;
        }
        memcpy(key,ctx->string,strlen(ctx->string));
        keys[i++] = key;

        cJSON* def = ctx;
        // handle aliases
        int len = strlen(ctx->string);
        char* keyy = malloc((len+suffixLen+2)*sizeof(char));
	memset(keyy, '\0', (len+suffixLen+2)*sizeof(char));
        strcat(keyy,termSuffix);
        strcat(keyy,":");
        strcat(keyy,ctx->string);
        if(def->type == cJSON_String && strcmp(def->valuestring,"@id")==0){
            add_aliases_data(contextData->aliases,"id",keyy);
        }else if(def->type == cJSON_String && strcmp(def->valuestring,"@type")==0){
            add_aliases_data(contextData->aliases,"type",keyy);
        }
    }
    // assign id to key terms
    sort(keys,size);
    for (int i = 0; i < size; i++)
    {
        add_termMap_id(contextData->termMap,keys[i],contextData->current_id);
        contextData->current_id +=2;
    }
    
    // aliases 
    cJSON_ArrayForEach(ctx,context){
        cJSON* def = ctx;
        int len = strlen(ctx->string);
        // BUG: +2 why we need extra 1 byte?
        char* key = malloc((len+suffixLen+2)*sizeof(char));
        memset(key, '\0', (len+suffixLen+2)*sizeof(char));
        strcat(key,termSuffix);
        strcat(key,":");
        strcat(key,ctx->string);
        if((ctx->valuestring && strcmp(ctx->valuestring,"@id")==0) || has_aliases_entry(contextData->aliases,"id",key)){
            add_aliases_data(contextData->aliases,key,"id");
        }else if((ctx->valuestring && strcmp(ctx->valuestring,"@type")==0) || has_aliases_entry(contextData->aliases,"type",key)){
            add_aliases_data(contextData->aliases,key,"type");
        }
    }
    // Typed context 
    // xsd:"url"
    // xsd:dateTime
    // urldateTime
    cJSON_ArrayForEach(ctx,context){
        cJSON* def = ctx;
        int len = strlen(ctx->string);
        // BUG: +2 why we need extra 1 byte?
        char* key = malloc((len+1)*sizeof(char));
        memset(key, '\0', (len+1)*sizeof(char));
        strcat(key,ctx->string);
        cJSON* type = cJSON_GetObjectItem(def,"@type");
        if(type!=NULL && type->type == cJSON_String){
            // check for potential CURIE value
            int prefixLen = -1;
            char* value = type->valuestring;
            for (int i = 0; i < strlen(value); i++)
            {
                if(*(value+i)==':'){
                    prefixLen = i;
                    break;
                }
            }
            if(prefixLen!=-1){
                // may have curie
                char* prefix = malloc(prefixLen*sizeof(char));
                char* suffix = malloc((strlen(value)-prefixLen)*sizeof(char));
                for (int i = 0; i < prefixLen; i++)
                {
                    *(prefix+i) = *(value+i);
                }
                for (int i = 0; i < strlen(value)-prefixLen; i++)
                {
                    *(suffix+i) = *(value+i+prefixLen);
                }
                //TODO: O(n^2) just checking local context. need to see if parent had any defination as well
                cJSON* curie = cJSON_GetObjectItem(context,prefix);
                if(curie != NULL){
                    if(curie->type == cJSON_String){
                        int finalTypeLen = (strlen(value)-prefixLen+strlen(curie->valuestring));
                        char* finalType = malloc(finalTypeLen*sizeof(char));
                        strcat(finalType,curie->valuestring);
                        strcat(finalType,suffix+1);
                        add_termMap_dataType(contextData->termMap,key,get_dataType(finalType,contextData));
                    }else if(curie->type == cJSON_Object){
                        cJSON* id = cJSON_GetObjectItem(curie,"@id");
                        if(id!=NULL && id->type == cJSON_String){
                            int finalTypeLen = (strlen(value)-prefixLen+strlen(id->valuestring));
                            char* finalType = malloc(finalTypeLen*sizeof(char));
                            strcat(finalType,id->valuestring);
                            strcat(finalType,suffix+1);
                            add_termMap_dataType(contextData->termMap,key,get_dataType(finalType,contextData));
                        }
                    }
                }else{
                    add_termMap_dataType(contextData->termMap,key,get_dataType(value,contextData));
                }
                
            }
            
        }else{
           add_termMap_dataType(contextData->termMap,key,get_dataType(key,contextData));
        }
    }
    //scoped Context
    cJSON_ArrayForEach(ctx,context){
        cJSON* scopedContext = cJSON_GetObjectItem(ctx,"@context");
        if(scopedContext!=NULL){
            process_context(scopedContext,"",contextData);
        }
    }
}


// struct Term* get_term(char* term){
//     struct Term* termm = malloc(sizeof(struct Term));
//     termm->dataType = -1;
//     termm->id = 1;
//     return termm;
// };

// int set_term(struct Term* term){
//     return 1;
// };


// struct Term* get_term_from_id(int id){
//     struct Term* termm = malloc(sizeof(struct Term));
//     termm->dataType = -1;
//     termm->id = 1;
//     return termm; 
// };

// int get_set_context_url_id(char* url){
//     return 0;
// };


// char* get_context_url_from_id(int id){
//     char* c = malloc(sizeof(char));
//     return c;
// };
