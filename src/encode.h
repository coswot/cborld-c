#ifndef ENCODE_H
#define ENCODE_H extern
#include "../lib/cjson/cJSON.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "cbor.h"
#include "cbor/internal/builder_callbacks.h"
#include "cbor/internal/loaders.h"
#include "compress.h"

/**
 * @brief converts json-ld data to pure cbor data without any compression
 * 
 * @param json_buffer jsonld data
 * @param length length of the json_buffer
 * @param panic function to be called in case of error
 * @return cbor byte string
 */
extern char* encode_to_cbor_uncompressed(char* json_buffer,size_t* length,void *(*panic)(char *error));

/**
 * @brief converts json-ld data to compressed cbor-ld data. This needs to be called after the context is added with {add_context} 
 * else it will throw an error
 * 
 * 
 * @param json_buffer jsonld data
 * @param length length of the json_buffer
 * @param panic function to be called in case of error
 * @return cbor byte string
 */
char* encode_to_cbor_compressed(char* json_buffer,size_t* length,void* (*panic)(char* error),char* (*documentLoader)(char* url),char* contextMap);

#endif