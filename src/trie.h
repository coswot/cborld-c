

#ifndef TRIE
#define TRIE extern

#include <stdio.h>
#include <stdlib.h>
#include "term.h"
// Define the character size
#define CHAR_SIZE 60
 
// Data structure to store a Trie node


struct Trie
{
    int isLeaf;             // 1 when the node is a leaf node
    struct Term data;                     // for leaf node
    struct Trie* character[CHAR_SIZE];
};
// Function that returns a new Trie node
struct Trie* getNewTrieNode();
 
// Iterative function to insert a string into a Trie
void insert(struct Trie *head, char* str,struct Term data);
 
// Iterative function to search a string in a Trie. It returns id
// if the string is found in the Trie; otherwise, it returns -1.
struct Term search(struct Trie* head, char* str);
 
// Recursive function to delete a string from a Trie
int deletion(struct Trie **curr, char* str);
#endif