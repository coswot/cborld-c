#ifndef CBORSEARCH
#define CBORSEARCH extern

#include <cbor.h>
#include "cbor/internal/builder_callbacks.h"
#include "cbor/internal/loaders.h"
#include "LinkedList.h"

// By defaul ID is encoded as 0
#define ID_ENCODED_AS_INT 0

// cbor_compare return values
#define CBOR_COMPARE_DIFFERENT_TYPE -2
#define CBOR_COMPARE_LESS -1
#define CBOR_COMPARE_EQUAL 0
#define CBOR_COMPARE_MORE 1
#define CBOR_COMPARE_OTHER_MISMATCH -3


struct match {
    cbor_item_t* s; //pointers to variables or constant
    cbor_item_t* p;
    cbor_item_t* o;
};

//Query will have variables such as ?s ?p? ?o ?g
//variable will be represented by NULL
//constants will 
struct cbor_query {
    cbor_item_t* s; //pointers to variables or constant
    cbor_item_t* p;
    cbor_item_t* o;
    //List of the associated matches to this query
    LinkedList* match_list;
}; 

void print_query_results(LinkedList* query_list);
bool is_match(cbor_item_t* item, cbor_item_t* targetitem, uint8_t encoded_id_int);
cbor_item_t* get_all_matches_in_map(LinkedList* query_list, cbor_item_t* item, int encoded_id_int);
// Compares two items
// First match types and then value accordingly 
// This function is recursive
// Returns -3 for other mismatches not covered below
// Returns -2 if type is not same, 
// -1 if type is same and first is less than second, 
// 0 if equal 
// 1 if first is more than second
int cbor_compare(cbor_item_t* item, cbor_item_t* targetitem, uint8_t encoded_id_int);
#endif

