#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "compress.h"
typedef struct Term Term;

int error = 0;

//detect datatimestamp like 2017-04-16T00:00:12+00:00
bool isPotentialDateTimeStamp(const char *dateTimeStr) {
    size_t len = strlen(dateTimeStr);
    if (len != 25) return false;
    if (dateTimeStr[4] == '-' 
    &&  dateTimeStr[7] == '-' 
    && dateTimeStr[10] == 'T' 
    && dateTimeStr[13] == ':' 
    && dateTimeStr[16] == ':' 
    && (dateTimeStr[19] == '+' || dateTimeStr[19] == '-')
    && dateTimeStr[22] == ':')
           return true;
    return false;
}

bool isPotentialDateTime(const char *dateTimeStr) {
    size_t len = strlen(dateTimeStr);
    if (len != 20) return false;
    if (dateTimeStr[4] == '-' 
    &&  dateTimeStr[7] == '-' 
    && dateTimeStr[10] == 'T' 
    && dateTimeStr[13] == ':' 
    && dateTimeStr[16] == ':' 
    && dateTimeStr[19] == 'Z')
           return true;
    return false;
}

void _transform(cJSON *obj,cJSON** transformMap,char* (*documentLoader)(char* url),void *(*panic)(char *error),ContextData* activeContext);

void _transformTypedValue(cJSON *obj,cJSON** transformMap,int dataType,char* (*documentLoader)(char* url),ContextData* activeContext,void *(*panic)(char *error)){
   // *transformMap = obj;
    //memcpy(*transformMap,obj,sizeof(obj));
    //XX hack to force URI being encoded
    //if(dataType == DATA_Uri) dataType = -1;

    if(dataType == -1){
        if(obj->type == cJSON_String){
            if ((isPotentialDateTimeStamp(obj->valuestring) == true) 
            ||  (isPotentialDateTime(obj->valuestring) == true)) 
                dataType = DATA_XsdDateTime;
        }
    }
    if(dataType == -1){
        if(obj->type == cJSON_String){
            *transformMap = cJSON_CreateString(obj->valuestring);
        }else if(obj->type == cJSON_Number){
            *transformMap = cJSON_CreateNumber(obj->valuedouble);
        }
        else if(obj->type == cJSON_False) *transformMap = cJSON_CreateFalse();
        else if(obj->type == cJSON_True) *transformMap = cJSON_CreateTrue();
        else if(obj->type == cJSON_NULL) *transformMap = cJSON_CreateNull();
        else if(obj->type == cJSON_Object) {
            _transform(obj,transformMap,documentLoader,panic,activeContext);
        }else if(obj->type == cJSON_Array){
            _transform(obj,transformMap,documentLoader,panic,activeContext);
        }else{
            //invalid type
        }
    }else{
        if(dataType == DATA_Context) context_encoder(obj,transformMap,activeContext);
        else if(dataType == DATA_Multibase) multibase_encoder(obj,transformMap);
        else if(dataType == DATA_Type) type_encoder(obj,transformMap,activeContext);
        else if(dataType == DATA_Uri) uri_encoder(obj,transformMap,activeContext);
        else if(dataType == DATA_VocabTerm) vocab_encoder(obj,transformMap,activeContext);
        else if(dataType == DATA_XsdDate) xsdDate_encoder(obj,transformMap);
        else if(dataType == DATA_XsdDateTime) xsdDateTime_encoder(obj,transformMap);
        else{
            // invalid
        }
         
    }
    
}


void _transform(cJSON *obj,cJSON** transformMap,char* (*documentLoader)(char* url),void *(*panic)(char *error),ContextData* activeContext){
    /**
     *  Step 1 : Change all term to their respective ids
     *      Simple sort and assign
     *  Step 2 : Change ContextURL to their IDs
     *      From contextMap
     *  Step 3 : Change values to their codecs
     *      From documentLoader get the contextDocument process the contextDocument to find the type of terms
     *          i) URI Codecs - HTTP
     *          ii) Date Codec
     *          iii) DateTime Codec
     */
    //printf("\n %s \n",cJSON_Print(obj));
    if(obj->type == cJSON_Array){
        cJSON* cur;
        cJSON* children = cJSON_CreateArray();
        cJSON_ArrayForEach(cur,obj){
            cJSON* childMap;
            _transformTypedValue(cur,&childMap,-1,documentLoader,activeContext,panic);
            cJSON_AddItemToArray(children,childMap);
        }
        *transformMap = children;
    }else if(obj->type == cJSON_Object){
        // check for context
        cJSON* contexts = cJSON_GetObjectItem(obj,"@context");
        if(contexts!=NULL){
            if(contexts->type == cJSON_Array){
                cJSON* context;
                cJSON_ArrayForEach(context,contexts){
                    add_context(context->valuestring,documentLoader(context->valuestring),activeContext);
                }
            }else  add_context(contexts->valuestring,documentLoader(contexts->valuestring),activeContext);
        }
        cJSON* cur;
        cJSON* children = cJSON_CreateObject();
        cJSON_ArrayForEach(cur,obj){
            cJSON* childMap;
            int id = get_termMap_id(activeContext->termMap,cur->string);
            int dataType = get_termMap_dataType(activeContext->termMap,cur->string);
            if (id == -1) {
                id = get_contextMap_data(activeContext->contextMap,cur->string);
                //if (id!= -1) dataType = DATA_Uri;
            }
            _transformTypedValue(cur,&childMap,dataType,documentLoader,activeContext,panic);
            cJSON_AddItemReferenceToObject(children,
                          id==-1?cur->string:int_to_string_alloc(id), childMap); 
        }
        *transformMap = children;
    }else{
        *transformMap = obj;
    }
    //printf("\n----------------------------------\n%s\n",cJSON_Print(*transformMap));
}


cJSON* compress(cJSON* doc,char* (*documentLoader)(char* url),void *(*panic)(char *error),cJSON* contextMap){
    cJSON *transformMap;
    ContextData* activeContext = get_empty_context_data(contextMap);
    _transform(doc,&transformMap,documentLoader,panic,activeContext);
    return transformMap;
}
