#include "util.h"

char *int_to_string_alloc(int x) {
  int i = x;
  char buf[INT_DECIMAL_STRING_SIZE(int)];
  char *p = &buf[sizeof buf] - 1;
  *p = '\0';
  if (i >= 0) {
    i = -i;
  }
  do {
    p--;
    *p = (char) ('0' - i % 10);
    i /= 10;
  } while (i);
  if (x < 0) {
    p--;
    *p = '-';
  }
  size_t len = (size_t) (&buf[sizeof buf] - p);
  char *s = (char*) malloc(len);
  if (s) {
    memcpy(s, p, len);
  }
  return s;
}

static int cmpString(const void* a, const void* b)
{
 
    // setting up rules for comparison
    return strcmp(*(char**)a, *(char**)b);
}
 
// Function to sort the array
void sort(char* arr[], int n)
{
    // calling qsort function to sort the array
    // with the help of Comparator
    qsort(arr, n, sizeof(char*), cmpString);
}



int cJSON_HasElement(cJSON* arr,cJSON* element){
  cJSON* current;
  cJSON_ArrayForEach(current,arr){
    if(cJSON_Compare(current,element,0)) return 1;
  }
  return 0;
}