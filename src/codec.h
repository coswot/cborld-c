#ifndef CODEC
#define CODEC extern


#include "data.h"
#include "../lib/cjson/cJSON.h"
#include "time.h"


void uri_encoder(cJSON* obj,cJSON** transformMap, ContextData* activeContext);

void type_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext);

void context_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext);

void multibase_encoder(cJSON* obj,cJSON** transformMap);

void vocab_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext);

void xsdDate_encoder(cJSON* obj,cJSON** transformMap);

void xsdDateTime_encoder(cJSON* obj,cJSON** transformMap);


void uri_decoder(cJSON* obj,cJSON** transformMap);

void type_decoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext);

void context_decoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext);

void multibase_decoder(cJSON* obj,cJSON** transformMap);

void vocab_decoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext);

void xsdDate_decoder(cJSON* obj,cJSON** transformMap);

void xsdDateTime_decoder(cJSON* obj,cJSON** transformMap);

#endif