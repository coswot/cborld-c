#include "data.h"

int add_aliases_data(Aliases* data,char* key,char* value){
    //printf("\n%s - %s\n",key,value);
    if(strcmp(key,"id")==0){
        cJSON_AddItemToArray(data->id,cJSON_CreateString(value));
    }else{
        cJSON_AddItemToArray(data->type,cJSON_CreateString(value));
    }
};
int has_aliases_entry(Aliases* data,char* key,char* entry){
   // printf("\n%s \n",key);

    if(strcmp(key,"id")==0){
        return cJSON_HasElement(data->id,cJSON_CreateString(entry));
    }else{
        return cJSON_HasElement(data->type,cJSON_CreateString(entry));
    }
};

int add_termMap_id(TermMap* data,char* key,int value){
    //printf("\n%s - %d\n",key,value);
    cJSON_AddItemToObject(data->ids,key,cJSON_CreateNumber(value));
    return value;
};
int get_termMap_id(TermMap* data,char* key){
    //printf("%s",cJSON_Print(data->ids));
    cJSON* id = cJSON_GetObjectItem(data->ids,key);
    if(id==NULL) return -1;
    return id->valuedouble;
};
char* get_terMap_from_id(TermMap* data,int id){
    cJSON* cur;
    cJSON_ArrayForEach(cur,data->ids){
        if(cur->valueint == id){
            return cur->string;
        }
    }
    return "Invalid";
}

int add_termMap_dataType(TermMap* data,char* key,int value){
    printf("\n%s - %d\n",key,value);
    cJSON_AddItemToObject(data->dataType,key,cJSON_CreateNumber(value));
    return value;
};
int get_termMap_dataType(TermMap* data,char* key){
    cJSON* id = cJSON_GetObjectItem(data->dataType,key);
    if(id==NULL) return -1;
    return id->valuedouble;
};

int add_contextMap_data(ContextMap* data,char* key,int value){
    cJSON_AddItemToObject(data->map,key,cJSON_CreateNumber(value));
    return value;
};
int get_contextMap_data(ContextMap* data,char* key){
    cJSON* id = cJSON_GetObjectItem(data->map,key);
    if(id==NULL) return -1;
    return id->valuedouble;
};
char* get_contextMap_from_id(ContextMap* data,int id){
    cJSON* cur;
    cJSON_ArrayForEach(cur,data->map){
        if(cur->valueint == id){
            return cur->string;
        }
    }
    return "Invalid";
}

cJSON* load_default_term_map_ids(){
    char* defaults[5] = {"@id", "@graph", "@type", "@value", "@language"};
    //XXKamal making id as Data_type but need to correct uri_decoder
    int defDataType[5] = {DATA_Type,-1, DATA_Type, -1, -1};
    int sizeDef = 5;
    cJSON* def = cJSON_CreateObject();
    for (int i = 0; i < 5; i++)
    {
        cJSON_AddItemToObject(def,defaults[i],cJSON_CreateNumber(i));
    }
    return def;
}

cJSON* load_default_term_map_dataTypes(){
    char* defaults[5] = {"@id", "@graph", "@type", "@value", "@language"};
    //{"@context","@type","@id","@value","@direction","@graph","@included","@index","@json","@language","@list","@nest","@reverse","@base","@container","@default","@embed","@explicit","@none","@omitDefault","@prefix","@preserve","@protected","@requireAll","@set","@version","@vocab"};
    // TODO: Use smaller datatype
    //XXKamal making id as Data_type but need to correct uri_decoder
    int defDataType[5] = {DATA_Type,-1, DATA_Type, -1, -1};
    //DATA_Context,DATA_Type,DATA_Uri,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,DATA_VocabTerm};
    int sizeDef = 5;
    cJSON* def = cJSON_CreateObject();
    for (int i = 0; i < 5; i++)
    {
        cJSON_AddItemToObject(def,defaults[i],cJSON_CreateNumber(defDataType[i]));
    }
    return def;
}
ContextData* get_empty_context_data(cJSON* contextMap){
    ContextData* contextData = malloc(sizeof(ContextData));
    TermMap* termMap = malloc(sizeof(TermMap));
    termMap->ids = load_default_term_map_ids();
    termMap->dataType = load_default_term_map_dataTypes();
    contextData->termMap = termMap;
    Aliases* aliases = malloc(sizeof(Aliases));
    contextData->aliases = aliases;
    aliases->id = cJSON_CreateArray();
    aliases->type = cJSON_CreateArray();
    contextData->aliases = aliases;
    ContextMap* contextMapObj = malloc(sizeof(ContextMap));
    contextMapObj->map = contextMap;
    contextData->contextMap = contextMapObj;
    contextData->current_id = FIRST_CUSTOM_TERM_ID;
    return contextData;
}