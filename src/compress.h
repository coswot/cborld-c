#ifndef COMPRESS
#define COMPRESS extern


#include "../lib/cjson/cJSON.h"
#include "util.h"
#include "contexts.h"
#include "codec.h"

cJSON* compress(cJSON* doc,char* (*documentLoader)(char* url),void *(*panic)(char *error),cJSON* contextMap);


#endif