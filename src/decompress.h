#ifndef DECOMPRESS
#define DECOMPRESS extern


#include "../lib/cjson/cJSON.h"
#include "util.h"
#include "contexts.h"
#include "codec.h"

cJSON* decompress(cJSON* json,void *(*panic)(char *error),char* (*documentLoader)(char* url),cJSON* contextMap);


#endif