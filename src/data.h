
#ifndef DATA_H
#define DATA_H extern

#include <stdio.h>
#include <stdlib.h>
#include "../lib/cjson/cJSON.h"
#include <string.h>
#include "util.h"
typedef struct TermMap{
    cJSON* ids;
    cJSON* dataType;
}TermMap;

typedef struct Aliases{
    cJSON* id;
    cJSON* type;
}Aliases;
typedef struct ContextMap{
    cJSON* map;
}ContextMap;

typedef struct ContextData{
    TermMap* termMap;
    Aliases* aliases;
    ContextMap* contextMap;
    int current_id;
}ContextData;

#define FIRST_CUSTOM_TERM_ID 5000

#define DATA_Uri 0 // @id
#define DATA_Context 1 // @context
#define DATA_VocabTerm 2 // @vocab
#define DATA_Multibase 3 // https://w3id.org/security#multibase
#define DATA_XsdDate 4 // http://www.w3.org/2001/XMLSchema#date
#define DATA_XsdDateTime 5 // http://www.w3.org/2001/XMLSchema#dateTime
#define DATA_Type 6 


int add_aliases_data(Aliases* data,char* key,char* value);
int has_aliases_entry(Aliases* data,char* key,char* entry);

int add_termMap_id(TermMap* data,char* key,int value);
int get_termMap_id(TermMap* data,char* key);


int add_termMap_dataType(TermMap* data,char* key,int value);
int get_termMap_dataType(TermMap* data,char* key);

int add_contextMap_data(ContextMap* data,char* key,int value);
int get_contextMap_data(ContextMap* data,char* key);

ContextData* get_empty_context_data(cJSON* contextMap);

char* get_terMap_from_id(TermMap* data,int id);

char* get_contextMap_from_id(ContextMap* data,int id);

#endif