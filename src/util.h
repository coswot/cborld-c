#ifndef UTIL
#define UTIL extern

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../lib/cjson/cJSON.h"

#define CHAR_BIT 8
#define INT_DECIMAL_STRING_SIZE(int_type) ((CHAR_BIT*sizeof(int_type)-1)*10/33+3)


char *int_to_string_alloc(int x) ;

static int cmpString(const void* a, const void* b);
 
// Function to sort the array
void sort(char* arr[], int n);

int cJSON_HasElement(cJSON* arr,cJSON* element);



#endif // UTIL