/**
 * Store the context objects here
 * Aim to process the context Documents to store relevent data
 * Store termMap - url:[{Key: DataType},{Key: DataType},{Key: DataType}]
 * 
 */

#include "trie.h"
#include <string.h>
#include "data.h"
#include "util.h"
#include "../lib/cjson/cJSON.h"

// /**
//  * @brief Get the [Term] object from the name of the term from the Trie in memory
//  * 
//  * @param term
//  * @return struct Term* 
//  */
// struct Term* get_term(char* term);

// /**
//  * @brief sets custom term in the trie.
//  * 
//  * @param term 
//  * @return int
//  */
// int set_term(struct Term* term);

// /**
//  * @brief Get the [Term] object from id of the term
//  * 
//  * @param id 
//  * @return struct Term* 
//  */
// struct Term* get_term_from_id(int id);


// /**
//  * @brief Get or set if abscent context url to id and return id else -1
//  * 
//  * @param url Context Url 
//  * @return Mapped id
//  */
// int get_set_context_url_id(char* url);

// /**
//  * @brief Get the context url from id
//  * 
//  * @param id 
//  * @return context url
//  */
// char* get_context_url_from_id(int id);

/**
 * @brief This processes the context object and saves the term with their id and datatype to be used during encoding and decoding
 * 
 * @param contextUrl Unique url of the contextDocument
 * @param contextDocument Json-ld based context document - Needs to be consistent while encoding and decoding
 * @return 0/1
 */
int add_context(char* contextUrl,char* contextDocument,ContextData* contextData);

void process_context(cJSON* context,char* termSuffix,ContextData* contextData);
