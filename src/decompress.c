#include "decompress.h"

typedef struct Term Term;

//kamal: this double declaration of error = 0 was giving a compile error
//int error = 0;

void _transform_de(cJSON *obj,cJSON** transformMap,char* (*documentLoader)(char* url),void *(*panic)(char *error),ContextData* activeContext);

void _transformTypedValue_de(cJSON *obj,cJSON** transformMap,int dataType,char* (*documentLoader)(char* url),ContextData* activeContext,void *(*panic)(char *error)){
   // *transformMap = obj;
    //memcpy(*transformMap,obj,sizeof(obj));
    if(dataType == -1){
        if(obj->type == cJSON_String){
            *transformMap = cJSON_CreateString(obj->valuestring);
        }else if(obj->type == cJSON_Number){
            *transformMap = cJSON_CreateNumber(obj->valuedouble);
        }
        else if(obj->type == cJSON_False) *transformMap = cJSON_CreateFalse();
        else if(obj->type == cJSON_True) *transformMap = cJSON_CreateTrue();
        else if(obj->type == cJSON_NULL) *transformMap = cJSON_CreateNull();
        else if(obj->type == cJSON_Object) {
            _transform_de(obj,transformMap,documentLoader,panic,activeContext);
        }else if(obj->type == cJSON_Array){
            _transform_de(obj,transformMap,documentLoader,panic,activeContext);
        }else{
            //invalid type
            //lets try transform Map
             _transform_de(obj,transformMap,documentLoader,panic,activeContext);
        }
    }else{
        if(dataType == DATA_Context) context_decoder(obj,transformMap,activeContext);
        else if(dataType == DATA_Multibase) multibase_decoder(obj,transformMap);
        else if(dataType == DATA_Type) type_decoder(obj,transformMap,activeContext);
        else if(dataType == DATA_Uri) uri_decoder(obj,transformMap);
        else if(dataType == DATA_VocabTerm) vocab_decoder(obj,transformMap,activeContext);
        else if(dataType == DATA_XsdDate) xsdDate_decoder(obj,transformMap);
        else if(dataType == DATA_XsdDateTime) xsdDateTime_decoder(obj,transformMap);
        else{
            // invalid
            //lets try transform map
             _transform_de(obj,transformMap,documentLoader,panic,activeContext);
        }
         
    }
    
}


void _transform_de(cJSON *obj,cJSON** transformMap,char* (*documentLoader)(char* url),void *(*panic)(char *error),ContextData* activeContext){
    printf("\n %s \n",cJSON_Print(obj));
    if(obj->type == cJSON_Array){
        cJSON* cur;
        cJSON* children = cJSON_CreateArray();
        cJSON_ArrayForEach(cur,obj){
            cJSON* childMap;
            _transformTypedValue_de(cur,&childMap,-1,documentLoader,activeContext,panic);
            cJSON_AddItemToArray(children,childMap);
        }
        *transformMap = children;
    }else if(obj->type == cJSON_Object){
        // check for context
        //assuming that context is encoded as "9"
        cJSON* contexts = cJSON_GetObjectItem(obj,"9");
        if(contexts!=NULL){
            if(contexts->type == cJSON_Array){
                cJSON* context;
                cJSON_ArrayForEach(context,contexts){
                    char* url = get_contextMap_from_id(activeContext->contextMap,context->valueint);
                    add_context(url,documentLoader(url),activeContext);
                }
            }else  {
                char* url = get_contextMap_from_id(activeContext->contextMap,contexts->valueint);
                add_context(url,documentLoader(url),activeContext);
            }
        }
        cJSON* cur;
        cJSON* children = cJSON_CreateObject();
        cJSON_ArrayForEach(cur,obj){
            cJSON* childMap;
            int id = atoi(cur->string);
            //Kamal adding check for id < 10
            if (id < 10) {
                char* key = get_terMap_from_id(activeContext->termMap,id);
                //int dataType = -1;
                int dataType = get_termMap_dataType(activeContext->termMap,key);
                _transformTypedValue_de(cur,&childMap,dataType,documentLoader,activeContext,panic);
                cJSON_AddItemReferenceToObject(children,key,childMap); 
            }else{
                char* key = get_contextMap_from_id(activeContext->contextMap,id);
                int dataType = -1; //get_termMap_dataType(activeContext->termMap,key);
                _transformTypedValue_de(cur,&childMap,dataType,documentLoader,activeContext,panic);
                cJSON_AddItemReferenceToObject(children,key,childMap); 
           
            }
        }
        *transformMap = children;
    }else{
        *transformMap = obj;
    }
 //   printf("\n----------------------------------\n%s\n",cJSON_Print(*transformMap));
}


cJSON* decompress(cJSON* doc,void *(*panic)(char *error),char* (*documentLoader)(char* url),cJSON* contextMap){
    cJSON *transformMap;
    ContextData* activeContext = get_empty_context_data(contextMap);
    _transform_de(doc,&transformMap,documentLoader,panic,activeContext);
    return transformMap;
}
