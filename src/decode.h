
#include "../lib/cjson/cJSON.h"
#include <float.h>
#include <math.h>
#include <string.h>
#include "cbor.h"
#include "cbor/internal/builder_callbacks.h"
#include "cbor/internal/loaders.h"
#include "util.h"
#include "decompress.h"
/**
 * @brief Treats cbor-ld data as pure cbor and converts to json format without decompression. Works normally for data
 * which was encoded without compression
 * 
 * @param cborBuffer cborld data
 * @param length length of the json_buffer
 * @param panic function to be called in case of error
 * @return cbor byte string
 */
extern char* decode_to_cbor_uncompressed(char* cborBuffer,size_t* length,void *(*panic)(char *error));

/**
 * @brief converts cbor-ld data to uncompressed json-ld data. This needs to be called after the context is added with {add_context} 
 * else it will throw an error
 * 
 * 
 * @param cborBuffer cborld data
 * @param length length of the json_buffer
 * @param panic function to be called in case of error
 * @return cbor byte string
 */
extern char* decode_to_cbor_compressed(char* cborBuffer,size_t* length,void *(*panic)(char *error));


cJSON* decode_to_json(char* cbor_buffer,size_t* cbor_length,void *(*panic)(char *error));

cJSON* decompress_decode_to_json(char* cbor_buffer,size_t* cbor_length,void *(*panic)(char *error),char* (*documentLoader)(char* url),char* contextMap);

struct String
{
    char* value;
    size_t size;
};

typedef struct String String;

struct Triple
{
    String* subject;
    int predicate;
    String* object;
    struct Triple* next;
};
typedef struct Triple Triple;
// {}{}{}
/**
 * @brief 
 * 
 * @param cbor_buffer 
 * @param length 
 * @param pattern - NULL for all values
 * @return Array of Triples
 */
char* match_tp(char* cbor_buffer,size_t length,Triple pattern,char** triples,size_t* len);