#include "codec.h"

void uri_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){
    char* uri = obj->valuestring;
    int http = 1,https = 1;
    char* httpString = "http://";
    char* httpsString = "https://";
    int uriLen = strlen(uri);

    int id = get_contextMap_data(activeContext->contextMap,obj->valuestring);
    
    if(id != -1){
        //XX Kamal: we found direct encoding of the URI
        //printf("================Found");
            *transformMap = cJSON_CreateNumber(id);
            return;
    }      

    if(uriLen<7){
        http = 0;
    }else{
         for (int i = 0; i < 7; i++)
        {
            if(httpString[i]!=uri[i]) http = 0;
        }
    }
    if(uriLen<8){
        https = 0;
    }else{
        for (int i = 0; i < 8; i++)
        {
            if(httpsString[i]!=uri[i]) https = 0;
        }
    }
    if(https){
        *transformMap = cJSON_CreateArray();
        cJSON_AddItemToArray(*transformMap,cJSON_CreateNumber(2));
        cJSON_AddItemToArray(*transformMap,cJSON_CreateString(uri+8));
    }else if(http){
        
        *transformMap = cJSON_CreateArray();
        cJSON_AddItemToArray(*transformMap,cJSON_CreateNumber(1));
        cJSON_AddItemToArray(*transformMap,cJSON_CreateString(uri+7));
    }else{
        // TODO: handle different URI types
        *transformMap = cJSON_CreateString(uri);
    }
    
};
// @type - term or uri
void type_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){
    if(obj->type == cJSON_Array){
        cJSON* cur;
        cJSON* children = cJSON_CreateArray();
        cJSON_ArrayForEach(cur,obj){
            cJSON* child;
            type_encoder(cur,&child,activeContext);
            cJSON_AddItemToArray(children,child);
        }
        *transformMap = children;
    }else{
        int id = get_termMap_id(activeContext->termMap,obj->valuestring);
        if(id==-1){
            uri_encoder(obj,transformMap, activeContext);
        }else{
            *transformMap  = cJSON_CreateNumber(id);
        }
    }
    
};

void context_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){
    if(obj->type == cJSON_Array){
        cJSON* cur;
        cJSON* children = cJSON_CreateArray();
        cJSON_ArrayForEach(cur,obj){
            cJSON* child;
            context_encoder(cur,&child,activeContext);
            cJSON_AddItemReferenceToArray(children,child);
        }
        *transformMap = children;
    }else if(obj->type == cJSON_String){
        int id = get_contextMap_data(activeContext->contextMap,obj->valuestring);
        if(id==-1){
            *transformMap = cJSON_CreateString(obj->valuestring);
        }else *transformMap = cJSON_CreateNumber(id);
    }else{
        //error
        printf("NON URL @CONTEXT, PLEASE USE UNCOMPRESSED MODE");
    }
};

void multibase_encoder(cJSON* obj,cJSON** transformMap){

};

void vocab_encoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){
    type_encoder(obj,transformMap,activeContext);
};

void xsdDate_encoder(cJSON* obj,cJSON** transformMap){
    struct tm tmVar;
    time_t timeVar;
    char* value = obj->valuestring;
    sscanf(value,"%d-%d-%d",&tmVar.tm_year,&tmVar.tm_mon, &tmVar.tm_mday);
    tmVar.tm_isdst = 1;
    tmVar.tm_year -= 1900;
    tmVar.tm_mon--;
    timeVar = mktime(&tmVar);
    // TODO: Dates before 1970 needs to be coded as negative integers
    if(timeVar == -1){
        *transformMap = cJSON_CreateString(obj->valuestring);
    }else *transformMap = cJSON_CreateNumber(timeVar);
};

void xsdDateTime_encoder(cJSON* obj,cJSON** transformMap){
    struct tm tmVar;
    time_t timeVar;
    char* value = obj->valuestring;
    sscanf(value,"%d-%d-%dT%d:%d:%d",&tmVar.tm_year,&tmVar.tm_mon, &tmVar.tm_mday,&tmVar.tm_hour,&tmVar.tm_min,&tmVar.tm_sec);
    tmVar.tm_isdst = 1;
    tmVar.tm_year -= 1900;
    tmVar.tm_mon--;
    timeVar = mktime(&tmVar);
    if(timeVar == -1){
        *transformMap = cJSON_CreateString(obj->valuestring);
    }else *transformMap = cJSON_CreateNumber(timeVar);
};



void uri_decoder(cJSON* obj,cJSON** transformMap){
    int https = 0; //XXX cJSON_GetArrayItem(obj,0)->valueint;
    char* uri = NULL; //XXX cJSON_GetArrayItem(obj,1)->valuestring;
    char* httpString = "http://";
    char* httpsString = "https://";
    int uriLen = 0;//XXX strlen(uri);
    char* finalString = NULL;
    
   //XXX
    if (cJSON_GetArrayItem(obj,0) != NULL){
	    https = cJSON_GetArrayItem(obj,0)->valueint;
    }
    if (cJSON_GetArrayItem(obj,1) != NULL){
	    uri = cJSON_GetArrayItem(obj,1)->valuestring;
    }//XXX 

    if(uri) {
        uriLen = strlen(uri);

        if(https){
            finalString = malloc((uriLen + 8)*sizeof(char));
            strcat(finalString,httpsString);
            strcat(finalString,uri);
        }else{
            finalString = malloc((uriLen + 7)*sizeof(char));
            strcat(finalString,httpString);
            strcat(finalString,uri);
        }
        //XXX
        *transformMap  = cJSON_CreateString(finalString);
    }
    else {
        //XXX bug uri is NULL. Right now returning valuestring to avoid problems
        *transformMap  = cJSON_CreateString(obj->valuestring);
    }  
};

void type_decoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){
    if(obj->type == cJSON_Array){
        // check for uri 
        if(cJSON_GetArraySize(obj)==2 && cJSON_GetArrayItem(obj,0)->type==cJSON_Number &&cJSON_GetArrayItem(obj,1)->type==cJSON_String){
            uri_decoder(obj,transformMap);
        }else{
            cJSON* cur;
            cJSON* children = cJSON_CreateArray();
            cJSON_ArrayForEach(cur,obj){
                cJSON* child;
                type_decoder(cur,&child,activeContext);
                cJSON_AddItemToArray(children,child);
            }
            *transformMap = children;
        }
    }else if(obj->type == cJSON_String){//XXX to solve bug of id, right now did: is not compressed
        *transformMap  = cJSON_CreateString(obj->valuestring);
    }else{
        int id = obj->valueint;
        char* key = NULL;
        if (id < 10) {//KAMAL
            key = get_terMap_from_id(activeContext->termMap, id);
        }else{
            key = get_contextMap_from_id(activeContext->contextMap,id);
        }
        if(strlen(key)==0){
            uri_decoder(obj,transformMap);
        }else{
            *transformMap  = cJSON_CreateString(key);
        }
    }
};

void context_decoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){
    if(obj->type == cJSON_Array){
        cJSON* cur;
        cJSON* children = cJSON_CreateArray();
        cJSON_ArrayForEach(cur,obj){
            cJSON* child;
            context_decoder(cur,&child,activeContext);
            cJSON_AddItemReferenceToArray(children,child);
        }
        *transformMap = children;
    }else if(obj->type == cJSON_Number){
        char* url = get_contextMap_from_id(activeContext->contextMap,obj->valueint);
        if(strcmp(url,"Invalid")==0){
            *transformMap = cJSON_CreateNumber(obj->valueint);
        }else *transformMap = cJSON_CreateString(url);
    }else{
        //error
        printf("NON URL @CONTEXT, PLEASE USE UNCOMPRESSED MODE");
    }
};

void multibase_decoder(cJSON* obj,cJSON** transformMap){

};

void vocab_decoder(cJSON* obj,cJSON** transformMap,ContextData* activeContext){

};

void xsdDate_decoder(cJSON* obj,cJSON** transformMap){
    if(obj->type == cJSON_Number){
        time_t rawtime = obj->valueint;
        struct tm  ts;
        char buf[80];

        // Format date, "ddd yyyy-mm-dd" 1958-07-17
        ts = *localtime(&rawtime);
        strftime(buf, sizeof(buf), "%Y-%m-%d", &ts);
        *transformMap = cJSON_CreateString(buf);
    }else{
        *transformMap = obj;
    }
};

void xsdDateTime_decoder(cJSON* obj,cJSON** transformMap){
    if(obj->type == cJSON_Number){
        time_t rawtime = obj->valueint;
        struct tm  ts;
        char buf[80];

        // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz" 1958-07-17T12:19:52Z
        ts = *localtime(&rawtime);
        strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%SZ%Z", &ts);
        *transformMap = cJSON_CreateString(buf);
    }else{
        *transformMap = obj;
    }
};
