#include "decode.h"
// TODO: change name of context for json-ld contextdoc and context for libcbor
struct Context{
  char* buffer;
  size_t bytes_read;
  size_t length;
  struct cbor_callbacks callbacks;
  struct cJSON** json;
  bool callback;
  void (*result_callback)(void* ctx);
  int size;
};

typedef struct Context Context;
struct cJsonOut{
  struct cJsonOut* parent;
  cJSON* cur;
  int type;
  bool haveParent;
  int currentcJsonLengthLeft;
};

struct Error{
  char* error;
  bool isError;
};

void setError(void* ctx, char* error){
  Context* context = (Context*) ctx;
 // context->error->error = error;
  //context->error->isError = true;
}

/*UNSIGNED INTEGER CALLBACKS*/
void cbor_uint64_callback(void* ctx,uint64_t value){
   Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateNumber(value);
  *context->json = parent;
}

void cbor_uint8_callback(void* ctx,uint8_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}
void cbor_uint16_callback(void* ctx,uint16_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
  }
void cbor_uint32_callback(void* ctx,uint32_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}


/*NEGATIVE INTEGER CALLBACKS*/
void cbor_negint8_callback(void* ctx,uint8_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}
void cbor_negint16_callback(void* ctx,uint16_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}
void cbor_negint32_callback(void* ctx,uint32_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}
void cbor_negint64_callback(void* ctx,uint64_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}

/*BYTE STRING CALLBACKS*/
void cbor_byte_string_callback(void* ctx,cbor_data value,size_t size){
   Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  //There is a bug libcbor ends the string using unicode but cJSON assumes that strings terminate with '\0'. I am creating a new CreateString function
  //parent = cJSON_CreateString(value);
  parent = cJSON_CreateStringLen(value, size);
  *context->json = parent;
}
void cbor_byte_string_start_callback(void* ctx){
  Context* context = (Context*) ctx;
  setError(context,"UNIMPLEMENTED Indefinite Byte strings");
}

/*STRING CALLBACKS*/
void cbor_string_type_callback(void* ctx,cbor_data value,size_t size){
  Context* context = (Context*) ctx;
  cbor_byte_string_callback(context,value,size);
}
void cbor_string_start_callback(void* ctx){
  Context* context = (Context*) ctx;
  setError(context,"UNIMPLEMENTED Indefinite strings");
}

/*ARRAY CALLBACKS*/


void cbor_array_start_callback_res(void* ctx){
  Context* context = (Context*) ctx;
  // here we need to create an array and for {size} time recursively 
  // add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateArray();
  cJSON* value;
  int size = context->size; //XXX preserve
  //value = *context->json;//XXX initialising it due to seg fault
  for (int i = 0; i < size; i++) //XXX removed context->size
  {
    context->json = &value;
    struct cbor_decoder_result result = cbor_stream_decode(context->buffer+context->bytes_read,
	   context->length - context->bytes_read,&context->callbacks,context);
    context->bytes_read += result.read;
    if(context->callback){ 
    	context->callback = false;
    	context->result_callback(context);
    }
    value = *context->json; //XXX
    cJSON_AddItemToArray(parent,value);
  }
  context->bytes_read = context->bytes_read;
  *context->json = parent;
  context->callback = false;
}


void cbor_array_start_callback(void* ctx,size_t size){
  Context* context = (Context*) ctx;
  context->callback = true;
  context->result_callback = &cbor_array_start_callback_res;
  context->size = size;
}



void cbor_indef_array_start_callback(void* ctx){
  Context* context = (Context*) ctx;
    setError(context,"UNIMPLEMENTED Indefinite array");
}

/*MAPS CALLBACKS*/


//need to call this after we get the result
void cbor_map_start_callback_after_res(void* ctx){
  Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateObject();
  cJSON* key;
  cJSON* value; 
  int size = context->size; //preserve
  for (int i = 0; i < size*2; i++)
  {
    if(i%2) {
      context->json = &value;
    }else context->json = &key;
    struct cbor_decoder_result result = cbor_stream_decode(context->buffer+context->bytes_read,context->length - context->bytes_read,&context->callbacks,context);
    context->bytes_read += result.read;
    if(context->callback) {
      context->callback = false;
      context->result_callback(context);
    }
    value = *context->json;
    if(i%2){
      char* keyString;
      if(key->type == cJSON_Number){
        keyString = int_to_string_alloc(key->valueint);
      }else keyString = key->valuestring;
      cJSON_AddItemToObject(parent,keyString,value);
    }
  }
  *context->json = parent;
  
}
// MAP5 - 5 item 
void cbor_map_start_callback(void* ctx,size_t size){
  Context* context = (Context*) ctx;
  context->callback = true;
  context->result_callback = &cbor_map_start_callback_after_res;
  context->size = size;
}

void cbor_indef_map_start_callback(void* ctx){
  Context* context = (Context*) ctx;
  setError(context,"UNIMPLEMENTED Indefinite map");
}

/*TAG CALLBACKS*/
void cbor_tag_callback(void* ctx,uint64_t value){
  Context* context = (Context*) ctx;
  cbor_uint64_callback(context,value);
}

/*FLOAT and MISC CALLBACKS*/

void cbor_float8_callback(void* ctx,double value){
  Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateNumber(value);
  *context->json = parent;
}
void cbor_float2_callback(void* ctx,float value){
  Context* context = (Context*) ctx;
  cbor_float8_callback(context,value);
}
void cbor_float4_callback(void* ctx,float value){
  Context* context = (Context*) ctx;
  cbor_float8_callback(context,value);
}
void cbor_undefined_callback(void* ctx){
   Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateNull();
  *context->json = parent;
}
void cbor_null_callback(void* ctx){
   Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateNull();
  *context->json = parent;
}
void cbor_boolean_callback(void* ctx,bool value){
   Context* context = (Context*) ctx;
  // here we need to create a map and for {size} time recursively add cJson objects to the object
  cJSON* parent;
  parent = cJSON_CreateBool(value);
  *context->json = parent;
}


/*INDEFINITE BREAK CALLBACKS*/
void cbor_indefinite_break_callback(void* ctx){
  Context* context = (Context*) ctx;
  setError(context,"UNIMPLEMENTED Indefinite BREAK");
}




// /*Call the processor after the previous value is processed*/
// void processor(void* ctx){
//   Context* context = (Context*) ctx;
//   if(context->bytes_read >= context->length) return;
//   if(context->error->isError) {
//     printf("\nPROCESSOR ERROR: %s\n",context->error->error);
//     return;
//   }
//   context->result = cbor_stream_decode(context->buffer+ context->bytes_read,context->length - context->bytes_read,&context->callbacks, context);
//   context->bytes_read += context->result.read;
//   //printf("BYTES: %ld,currentcJsonLengthLeft: %d ",context->bytes_read,context->json->currentcJsonLengthLeft);
//   bool haveParent = context->json->haveParent;
//   while(context->json->currentcJsonLengthLeft==0 && haveParent){
//     context->json = context->json->parent;
//     if(context->json->parent->haveParent) context->json->parent = context->json->parent->parent;
//     else{
//       context->json->haveParent = false;
//     }
//     haveParent = context->json->haveParent;
//   }
//   if(context->json->currentcJsonLengthLeft!=0) processor(context);
//   else{
//     context->json->haveParent = false;
//     context->json->parent = NULL;
//   }
// }


cJSON* decode_to_json(char* cbor_buffer,size_t* cbor_length,void *(*panic)(char *error)){  
    //TODO: check first 3 byte and call panic function
    cbor_buffer = cbor_buffer+3;
    *cbor_length -= 3;
    //printf("\nINPUT:%s\n",cbor_buffer);
    /* Convert between JSON and CBOR */
    struct cbor_callbacks callbacks = {
        /* Type 0 - Unsigned integers */
      .uint8 = &cbor_uint8_callback,
      .uint16 = &cbor_uint16_callback,
      .uint32 = &cbor_uint32_callback,
      .uint64 = &cbor_uint64_callback,

      /* Type 1 - Negative integers */
      .negint8 = &cbor_negint8_callback,
      .negint16 = &cbor_negint16_callback,
      .negint32 = &cbor_negint32_callback,
      .negint64 = &cbor_negint64_callback,

      /* Type 2 - Byte strings */
      .byte_string_start = &cbor_byte_string_start_callback,
      .byte_string = &cbor_byte_string_callback,

      /* Type 3 - Strings */
      .string_start = &cbor_string_start_callback,
      .string = &cbor_string_type_callback,

      /* Type 4 - Arrays */
      .indef_array_start = &cbor_indef_array_start_callback,
      .array_start = &cbor_array_start_callback,

      /* Type 5 - Maps */
      .indef_map_start = &cbor_indef_map_start_callback,
      .map_start = &cbor_map_start_callback,

      /* Type 6 - Tags */
      .tag = &cbor_tag_callback,

      /* Type 7 - Floats & misc */
      /* Type names cannot be member names */
      .float2 = &cbor_float2_callback,
      /* 2B float is not supported in standard C */
      .float4 = &cbor_float4_callback,
      .float8 = &cbor_float8_callback,
      .undefined = &cbor_undefined_callback,
      .null = &cbor_null_callback,
      .boolean = &cbor_boolean_callback,

      /* Shared indefinites */
      .indef_break = &cbor_indefinite_break_callback,

    };
    size_t length = *cbor_length;

    struct Error error;
    error.error = "";
    error.isError = false;

    cJSON* json;
    Context context = {
      .buffer = cbor_buffer,
      .length = length,
      .bytes_read = 0,
      .callbacks = callbacks,
      .json = &json,
    };
    
    struct cbor_decoder_result result;
    //cbor_item_t * item = cbor_load(cbor_buffer, length, &result);
    /* Pretty-print the result */
   // cbor_describe(item, stdout);
    result = cbor_stream_decode(cbor_buffer+ context.bytes_read,*cbor_length - context.bytes_read,&callbacks, &context);
    context.bytes_read += result.read;
    if(context.callback) {
      context.callback = false;
      context.result_callback(&context);
    }
    json = *context.json;
    // TODO: Being safe 
    char *jsonString = cJSON_Print(json);
    cJSON* finalJson = cJSON_Parse(jsonString);
    return finalJson;
}

cJSON* decompress_decode_to_json(char* cbor_buffer,size_t* cbor_length,void *(*panic)(char *error),char* (*documentLoader)(char* url),char* contextMap){
  cJSON* tansformMap = decode_to_json(cbor_buffer,cbor_length,panic);
  cJSON* cxtMap = cJSON_Parse(contextMap);
  cJSON* json = decompress(tansformMap,panic,documentLoader,cxtMap); 
  return json;
}

void subject_matching(char* cbor_buffer,size_t size,size_t length,Triple* pattern, char** last,size_t* triplesLen);
void object_matching(char* cbor_buffer,size_t size,size_t length,Triple* pattern, char** last,size_t* triplesLen,String* subject,int predicate);
void predicate_matching(char* cbor_buffer,size_t size,size_t length,Triple* pattern, char** last,size_t* triplesLen,String* subject);
void triple_finder(char* cbor_buffer,size_t length,Triple* pattern, char** last,size_t* triplesLen);
int get_next_cbor_value(char** cbor_buffer,size_t* length,void* value);


char* match_tp(char* cbor_buffer,size_t length,Triple pattern,char** triples,size_t* len){
  // subject maps to @id (4)
  // predicate is properties
  // object is value
  // @graph is mapped to 10
  // for each graph element with id maching to pattern.subject
  *triples = malloc(sizeof(char));
  char* head = *triples;
  triple_finder(cbor_buffer,length,&pattern,&head,len);
  *triples = head;
}

void triple_finder(char* cbor_buffer,size_t length,Triple* pattern, char** last,size_t* triplesLen){
  /*
  Step 1 : extract the graph element
  - search for 2 bytes,first 10 first 3bits of 2nd should be array and last 5bits as some {count}
  - extract the data enclosed by that {count}

  step 2:
  - for each {count} we must have a map of some {size}.
  - for each {2*size} we search for 04 byte in keys(even index)
  - if next item of 04 byte matches with subject we send this map to predicate and object matching
  - else move to next
  step 3 predicate matching:
  - we have a map with {size} 
  - for each {2*size} check for key(even index) where byte == predicaate if matches send to object match
  - else move to next
  step 4 object matching:
  - if matches append to triples
  */
 
  printf("%s",cbor_buffer);
  cbor_buffer += 3;
  char* graphmapPointer;
  int graphArrayLen = 0;
  printf("%s",cbor_buffer);
  for (int i = 0; i < length-1; i++)
  {
    uint8_t cur = *(cbor_buffer + i);
    uint8_t next = *(cbor_buffer+i+1);
    if(cur==10 && next>>5 == 4){
      cbor_buffer = cbor_buffer+i+2;
      length -= i+1;
      graphArrayLen = next&31;
      break;
    }
  }
  for (int i = 0; i < graphArrayLen; i++)
  {
    //this is a map
    int size = *cbor_buffer&31;
    subject_matching(cbor_buffer,size,length,pattern,last,triplesLen);
  }
}


void subject_matching(char* cbor_buffer,size_t size,size_t length,Triple* pattern, char** last,size_t* triplesLen){
  printf("\n");
  for (int i = 0; i < length; i++)
  {
      printf("%d ",(uint8_t)cbor_buffer[i]);
  }
  printf("\n");
  // we get a map here with some {size}
  // size is the count of element not number of byte (length)
  // this function should update the cbor_buffer to point to end of map
  char* original_cbor_buffer_pointer = cbor_buffer;
  size_t original_size = size;
  size_t original_len = length;
  // to skip map header
  cbor_buffer++;
  length--;
  for (int i = 0; i < 2*size; i++)
  {
    if(i%2==0){
      //key
      int64_t value;
      get_next_cbor_value(&cbor_buffer,&length,&value);
      if(value == 4){ // @id - 04
        //check for the next byte if it matches to pattern.subject
        String* id = malloc(sizeof(String));
        get_next_cbor_value(&cbor_buffer,&length,&id);
        if( pattern->subject->size == 0 || strncmp(id->value,pattern->subject->value,id->size)==0){
          // send for predicate checking
          predicate_matching(original_cbor_buffer_pointer,original_size,original_len,pattern,last,triplesLen,id);
        }
        break;
      }
    }
  }
}

void predicate_matching(char* cbor_buffer,size_t size,size_t length,Triple* pattern, char** last,size_t* triplesLen,String* subject){
  printf("\n");
  for (int i = 0; i < length; i++)
  {
      printf("%c ",(uint8_t)cbor_buffer[i]);
  }
  printf("\n");
  // to skip map header
  cbor_buffer++;
  length--;
  for (int i = 0; i < 2*size; i++)
  {
    if(i%2==0){
      //key
      int64_t value;
      get_next_cbor_value(&cbor_buffer,&length,&value);
      if(value!=4 && (pattern->predicate== -1 || value == pattern->predicate)){ //check if it matches to pattern.predicate
        //send value for object matching
        object_matching(cbor_buffer,size,length,pattern,last,triplesLen,subject,value);
      }
    }else {
      struct cbor_decoder_result result = cbor_stream_decode(cbor_buffer,length,&cbor_empty_callbacks,NULL);
      cbor_buffer += result.read;
      length -= result.read;
      printf("\n");
      for (int i = 0; i < length; i++)
      {
          printf("%c ",(uint8_t)cbor_buffer[i]);
      }
      printf("\n");
    }
  }
}

void object_matching(char* cbor_buffer,size_t size,size_t length,Triple* pattern, char** triples,size_t* triplesLen,String* subject,int predicate){
  printf("\n");
  for (int i = 0; i < length; i++)
  {
      printf("%d ",(uint8_t)cbor_buffer[i]);
  }
  printf("\n");
  
  String* value = malloc(sizeof(String));
  get_next_cbor_value(&cbor_buffer,&length,&value);
  if(pattern->object->size != 0){
    if(length<pattern->object->size) return;
    for (int i = 0; i < pattern->object->size; i++)
    {
      if(*(value->value+i)!=*(pattern->object->value+i)) return;
    }
  }

  cbor_item_t* sub = cbor_build_stringn(subject->value,subject->size);
  cbor_item_t* predi = cbor_build_uint64(predicate);
  cbor_item_t* obj = cbor_build_stringn(value->value,value->size);
  size_t totalLen = 0;
  unsigned char* sub_cbor;
  size_t buffer_size, sub_len = cbor_serialize_alloc(sub, &sub_cbor, &buffer_size);
  unsigned char* predi_cbor;
  size_t predi_len = cbor_serialize_alloc(predi, &predi_cbor, &buffer_size);
  unsigned char* obj_cbor;
  size_t obj_len = cbor_serialize_alloc(obj, &obj_cbor, &buffer_size);
  *triplesLen += sub_len+predi_len+obj_len+3;
  *triples = realloc(*triples,(*triplesLen)*sizeof(char));
  strcat(*triples,sub_cbor);
  strcat(*triples,predi_cbor);
  strcat(*triples,obj_cbor);
}

void cbor_next_string_callback(void* ctx,cbor_data value,size_t size){
  String** val = (String**)ctx;
  (*val)->value = value;
  (*val)->size = size;
}

void cbor_next_int64_callback(void* ctx,uint64_t value){
  int64_t* val = (int64_t*)ctx;
  *val = value;
}

void cbor_next_int32_callback(void* ctx,uint32_t value){
  int64_t* val = (int64_t*)ctx;
  *val = value;
}

void cbor_next_int16_callback(void* ctx,uint16_t value){
  int64_t* val = (int64_t*)ctx;
  *val = value;
}

void cbor_next_int8_callback(void* ctx,uint8_t value){
  int64_t* val = (int64_t*)ctx;
  *val = value;
}


int get_next_cbor_value(char** cbor_buffer,size_t* length,void* value){
  printf("\n");
  for (int i = 0; i < *length; i++)
  {
      printf("%c ",(uint8_t)(*cbor_buffer)[i]);
  }
  printf("\n");
  struct cbor_callbacks callbacks = {
      .uint8 = &cbor_next_int8_callback,
      .uint16 = &cbor_next_int16_callback,
      .uint32 = &cbor_next_int32_callback,
      .uint64 = &cbor_next_int64_callback,
      .byte_string = &cbor_next_string_callback,
      .string = &cbor_next_string_callback,
    };
    struct cbor_decoder_result result = cbor_stream_decode(*cbor_buffer,*length,&callbacks,value);
    *length -= result.read;
    *cbor_buffer += result.read;
    printf("\n");
    for (int i = 0; i < *length; i++)
    {
        printf("%c ",(uint8_t)(*cbor_buffer)[i]);
    }
    printf("\n");
}
