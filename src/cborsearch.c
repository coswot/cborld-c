#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <cbor.h>
#include "cbor/internal/builder_callbacks.h"
#include "cbor/internal/loaders.h"
#include "cbor/floats_ctrls.h"
#include "cborsearch.h"

#ifdef __GNUC__
#define UNUSED(x) __attribute__((__unused__)) x
#else
#define UNUSED(x) x
#endif


/*
is_match:
match types and then value accordingly

printmatchresults:
according to their type, print s, p, o

getmatch:
Iterate over all maps
	first get the subject from id
	iterate current map if id matches s, otherwise go to next map
  		p = key
  		o = value 
			if it is an array get array and set type = array
			If it is a map recursively search inside this map
				return subject value to be used for o
  		If (query.s == NULL or is_match(query.s, s)) 
 		and (query.p == NULL or query.p == p) 
  		and (query.o == NULL or query.o == p) 
			//store s, p, o
			match.s =s, match.p =p, match.o = o
			newmatch=>next = match
			match = newmatch
*/

//print the item for debugging
void print_cbor_item(cbor_item_t* item){
  switch (cbor_typeof(item)) {
    case CBOR_TYPE_UINT: //fallthrough
    case CBOR_TYPE_NEGINT:
      printf("%d ", cbor_get_int(item));
      break;
    case CBOR_TYPE_BYTESTRING://fallthrough //XXX need to handle byte string differently
    case CBOR_TYPE_STRING:
      if (cbor_string_is_indefinite(item)) {
        printf("indefinite not supported");
        return;
      } else  printf("%s ", (cbor_string_handle(item)));
      break;
    case CBOR_TYPE_ARRAY:
      printf("array:" );
      if (cbor_array_is_definite(item)) {
        for (size_t i = 0; i < cbor_array_size(item); i++) print_cbor_item(cbor_array_handle(item)[i]);
      } else printf("Indefinite not supported");
      break;
    case CBOR_TYPE_MAP:
      printf("Cant be a map! check for bugs! ");
      break;
    case CBOR_TYPE_TAG: 
      printf("Cant be a tag! check for bugs! ");
      break;
    case CBOR_TYPE_FLOAT_CTRL:
      printf("Cant be a float ctrl! check for bugs! ");
      break;
  
  }
}

void print_query_results(LinkedList* query_list){
  struct cbor_query* query = NULL;
  LinkedListNode* query_node = NULL;
  LinkedListNode* match_node = NULL;
  struct match *match = NULL;
  
  query_node = query_list->first;
  //Iterate over all the results of all queries
  while(query_node != NULL) {
      query = (struct cbor_query* )query_node->data;
      //Iterate over all matches of a query
      printf("\n Results of one of the query in the list:\n ");
      match_node = query->match_list->first;
      while(match_node != NULL) {
        match = (struct match* )match_node->data;
        printf("\n ");
        print_cbor_item(match->s);
        print_cbor_item(match->p);
        print_cbor_item(match->o);
        match_node = match_node->next;
        //printf("\n ");
      }
      // next query
      query_node = query_node->next;
  }
}

// Matches two items
// First match types and then value accordingly 
// This function is recursive           
bool is_match(cbor_item_t* item, cbor_item_t* targetitem, uint8_t encoded_id_int){
  cbor_type type = cbor_typeof(item);
  //Return false if already type doesnt match
  if (type != cbor_typeof(targetitem)) return false;

  switch (type) {
    case CBOR_TYPE_UINT: //fallthrough
    case CBOR_TYPE_NEGINT:
      return ((cbor_get_int(item) == cbor_get_int(targetitem)));
    case CBOR_TYPE_BYTESTRING://fallthrough//XXX need to handle it separately
    case CBOR_TYPE_STRING:
      if (cbor_string_is_indefinite(item)) {
        printf("indefinite not supported");
        return false;
      } else {
        if (cbor_string_length(item) != cbor_string_length(targetitem))
          return false;
        else {
          if (strncmp(cbor_string_handle(item), cbor_string_handle(targetitem),cbor_string_length(item)) == 0) 
            return true;
          else return false;
        } 
      }
    case CBOR_TYPE_ARRAY:
      if (cbor_array_is_definite(item)) {
        if (cbor_array_size(item) != cbor_array_size(targetitem)) return false;
        for (size_t i = 0; i < cbor_array_size(item); i++)
          if(cbor_array_handle(item)[i] != cbor_array_handle(targetitem)[i]) return false;
        
        //all matched if we are here
        return true;
      } else {
        printf("Indefinite not supported");
        return false;
      }
    case CBOR_TYPE_MAP:
      if (!cbor_map_is_definite(item)) {
        printf("Indefinite not supported");
        return false;
      }
      for (size_t i = 0; i < cbor_map_size(item); i++) {
        // check if key is id and value matches
        // id can be encoded as 4 or someother value
        // user should provide this value encoded_id
        if (is_match(cbor_map_handle(item)[i].key, cbor_map_handle(targetitem)[i].key, encoded_id_int))
            if ((cbor_get_int(cbor_map_handle(item)[i].key) == encoded_id_int || cbor_get_int(cbor_map_handle(item)[i].key) == ID_ENCODED_AS_INT) 
             && (is_match(cbor_map_handle(item)[i].value, cbor_map_handle(targetitem)[i].value, encoded_id_int)))
              return true;
      }
      //if we are here then no match was found
      return false;
    case CBOR_TYPE_TAG: 
      return false;//XXX to extend it to match tags too
    case CBOR_TYPE_FLOAT_CTRL:
      if (cbor_float_ctrl_is_ctrl(item)) {
        if (cbor_is_bool(item))
          printf("Float ctrls bool Not supported");
          //return (cbor_get_bool(item) == cbor_get_bool(targetitem));
        else if (cbor_is_undef(item)){
          printf("Undefined\n");
          return false;
        }
        else if (cbor_is_null(item))
          return (cbor_is_null(targetitem));
        else
          return(cbor_ctrl_value(item) == cbor_ctrl_value(targetitem));
      } else {
        return(cbor_float_get_float(item) == cbor_float_get_float(targetitem));
      }
  
  }
  //if we are here then nothing matched
  return false;
}


cbor_item_t* get_all_matches_in_map(LinkedList* query_list, cbor_item_t* item, int encoded_id_int){
  cbor_item_t* parent_subject = NULL;
  cbor_item_t* p;
  cbor_item_t* o;
  struct cbor_query* query;
  LinkedListNode* query_node = query_list->first;
  struct match *match = NULL;

  //assuming we are in the beginning of a map
  //return null if by error we dont have any map
  if (cbor_typeof(item) != CBOR_TYPE_MAP) return NULL;

  //iterate over all the current map 
  for (size_t i = 0; i < cbor_map_size(item); i++) {
    bool is_id = false;
    p = cbor_map_handle(item)[i].key;
    o = cbor_map_handle(item)[i].value;

    //If o is a map then we need to recursively search inside the map
    if (cbor_typeof(o) == CBOR_TYPE_MAP) {
      o = get_all_matches_in_map(query_list, o, encoded_id_int);
    }
    else if (cbor_typeof(o) == CBOR_TYPE_ARRAY) {
      for (size_t i = 0; i < cbor_array_size(o); i++){
        cbor_item_t* tmp = cbor_array_handle(o)[i];
        //process only if we have maps
        if (cbor_typeof(tmp) != CBOR_TYPE_MAP) break;
        tmp= get_all_matches_in_map(query_list, tmp, encoded_id_int);
      }//XXX processing an array will return last id as value. Need to check if it is useful.
      //o = NULL;
      //XXX normally we should return graph id when considering quads
    }
    //If the folowing is true then the value corresponds to subject
    if ((cbor_get_int(p) == encoded_id_int)||(cbor_get_int(p) == ID_ENCODED_AS_INT)) {
      parent_subject = o;
      is_id = true;
    }
    //Now match the query
    //XXX can be optimised, we dont need to check subject everytime

    //iterate over all queries in the list
    //start from first query in the list
    query_node = query_list->first;
    while(query_node != NULL) {
      query = (struct cbor_query* )query_node->data;

      if ((parent_subject != NULL)&&(p != NULL)&&(o != NULL) && (!is_id)
        &&((query->s == NULL) || is_match(query->s, parent_subject, encoded_id_int)) 
 		    &&((query->p == NULL) || is_match(query->p, p, encoded_id_int)) 
  		  &&((query->o == NULL) || is_match(query->o, o, encoded_id_int))) {
			  //store or print s, p, o
        //store_spo(query, parent_subject, p , o);
        match = (struct match *) malloc(sizeof(struct match));
        match->s = parent_subject;
        match->p = p;
        match->o = o;
        pushLinkedList(query->match_list, (void*)match);
        printf("\n ");
        print_cbor_item(parent_subject);
        print_cbor_item(p);
        print_cbor_item(o);
        //printf("\n ");
      }
      // next query
      query_node = query_node->next;
    }
  }
  return parent_subject;
}


// Compares two items
// First match types and then value accordingly 
// This function is recursive
// Returns -3 for other mismatches not covered below
// Returns -2 if type is not same, 
// -1 if type is same and first is less than second, 
// 0 if equal 
// 1 if first is more than second
int cbor_compare(cbor_item_t* item, cbor_item_t* targetitem, uint8_t encoded_id_int){
  cbor_type type = cbor_typeof(item);
  //Return false if already type doesnt match
  if (type != cbor_typeof(targetitem)) return CBOR_COMPARE_DIFFERENT_TYPE;

  switch (type) {
    case CBOR_TYPE_UINT: //fallthrough
    case CBOR_TYPE_NEGINT:
      if ((cbor_get_int(item) == cbor_get_int(targetitem))) return CBOR_COMPARE_EQUAL;
      else if ((cbor_get_int(item) < cbor_get_int(targetitem))) return CBOR_COMPARE_LESS;
      else if ((cbor_get_int(item) > cbor_get_int(targetitem))) return CBOR_COMPARE_MORE;
    case CBOR_TYPE_BYTESTRING://fallthrough//XXX need to handle it separately
    case CBOR_TYPE_STRING:
      if (cbor_string_is_indefinite(item)) {
        printf("indefinite not supported");
        return CBOR_COMPARE_OTHER_MISMATCH;
      } else {
        return strcmp(cbor_string_handle(item), cbor_string_handle(targetitem)); 
      }
    case CBOR_TYPE_ARRAY:
      if (cbor_array_is_definite(item)) {
        if (cbor_array_size(item) != cbor_array_size(targetitem)) return CBOR_COMPARE_OTHER_MISMATCH;
        for (size_t i = 0; i < cbor_array_size(item); i++)
          if(cbor_array_handle(item)[i] != cbor_array_handle(targetitem)[i]) return CBOR_COMPARE_OTHER_MISMATCH;;
        
        //all matched if we are here
        return CBOR_COMPARE_EQUAL;
      } else {
        printf("Indefinite not supported");
        return CBOR_COMPARE_OTHER_MISMATCH;
      }
    case CBOR_TYPE_MAP:
      if (!cbor_map_is_definite(item)) {
        printf("Indefinite not supported");
        return CBOR_COMPARE_OTHER_MISMATCH;
      }
      for (size_t i = 0; i < cbor_map_size(item); i++) {
        // check if key is id and value matches
        // id can be encoded as 4 or someother value
        // user should provide this value encoded_id
        if (is_match(cbor_map_handle(item)[i].key, cbor_map_handle(targetitem)[i].key, encoded_id_int))
            if ((cbor_get_int(cbor_map_handle(item)[i].key) == encoded_id_int || cbor_get_int(cbor_map_handle(item)[i].key) == ID_ENCODED_AS_INT) 
             && (is_match(cbor_map_handle(item)[i].value, cbor_map_handle(targetitem)[i].value, encoded_id_int)))
              return CBOR_COMPARE_EQUAL;
      }
      //if we are here then no match was found
      return CBOR_COMPARE_OTHER_MISMATCH;
    case CBOR_TYPE_TAG: 
      return CBOR_COMPARE_OTHER_MISMATCH;//XXX to extend it to match tags too
    case CBOR_TYPE_FLOAT_CTRL:
      if (cbor_float_ctrl_is_ctrl(item)) {
        if (cbor_is_bool(item))
          printf("Float ctrls bool Not supported");
          //return (cbor_get_bool(item) == cbor_get_bool(targetitem));
        else if (cbor_is_undef(item)){
          printf("Undefined\n");
          return CBOR_COMPARE_OTHER_MISMATCH;
        }
        else if ((cbor_is_null(item)) && (cbor_is_null(targetitem))) 
          return CBOR_COMPARE_EQUAL;
        else if (cbor_ctrl_value(item) == cbor_ctrl_value(targetitem))
          return CBOR_COMPARE_EQUAL;
        else if(cbor_float_get_float(item) == cbor_float_get_float(targetitem))
          return CBOR_COMPARE_EQUAL;
      }
  
  }
  //if we are here then nothing matched
  return CBOR_COMPARE_OTHER_MISMATCH;
}
