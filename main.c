#include <stdio.h>
#include <stdlib.h>
#include "src/encode.h"
#include "src/decode.h"
#include <string.h>
#include "src/cborsearch.h"

// This function will read  the contextmap file and return it as char* 
char* readFile(char* fileName,size_t* length){
    FILE *f = fopen(fileName, "rb");
    fseek(f, 0, SEEK_END);
    *length = (size_t)ftell(f);
    fseek(f, 0, SEEK_SET);
    char *json_buffer = malloc(*length + 1);
    fread(json_buffer, *length, 1, f);
    json_buffer[*length] = '\0';
    fclose(f);
    return json_buffer;
}

// Here one needs to load an appropriate context for the jsonld files to be compressed.
// This function will call the above readFile function.
// This function will be callback by encode and decode functions of CBORLD.
char* documentLoader(char* url){
    size_t length;
    //Here we assume some example context files.
    //For different contexts, one will have to create them manually.
    if(strcmp(url,"https://www.w3.org/2018/credentials/v1")==0){        
        char* context = readFile("../dictionaries/contextprc1.json",&length);
        return context;
    }else if(strcmp(url,"https://w3id.org/citizenship/v1")==0){        
        char* context = readFile("../dictionaries/contextprc2.json",&length);
        return context;
    }else if(strcmp(url,"http://example.org/triplet")==0){
        char* context = readFile("../dictionaries/contextTrip.json",&length);
        return context;
    }else if(strcmp(url,"https://liris.cnrs.fr/lionel.medini/temp/context-extern.jsonld")==0){
        char* context = readFile("../dictionaries/contextcoswot.json",&length);
        return context;
    }else if(strcmp(url,"http://www.w3.org/2002/07/owl#")==0){
        char* context = readFile("../dictionaries/contextowl.json",&length);
        return context;
    }else if(strcmp(url,"http://www.w3.org/2000/01/rdf-schema#")==0){
        char* context = readFile("../dictionaries/contextrdfs.json",&length);
        return context;
    }else if(strcmp(url,"http://example.org/lubm")==0){
        char* context = readFile("../dictionaries/contextlubm-pretty.json",&length);
        return context;
    }else{
        return "ads";
    }
}

//Helper function to write output files in binary format
int writeFileBinary(char* fileName,char* content,size_t cbor_length){
    FILE *fp;
    fp = fopen(fileName, "wb");
    if(fp == NULL) {
        printf("file can't be opened\n");
        return 1;
    }
    fwrite(content,1,cbor_length,fp);
    fclose(fp);
}

//Helper function to write outfile
int writeFile(char* fileName,char* content){
    FILE *fp;
    fp = fopen(fileName, "w");
    if(fp == NULL) {
        printf("file can't be opened\n");
        return 1;
    }
    fwrite(content,1,strlen(content),fp);
    fclose(fp);
}

void *panic(char* error){
    printf("%s",error);
}

/* @brief This is an example of how to use this cborld library
 * @params Takes 3 arguments <input file> <outputfile> <option>
 */

int main(int argc, char *argv[])
{ 
    //argv[1] = "../data/prc.jsonld";
    //argv[2] = "../output/out.cborld";
    //argv[3] = "-d";
    size_t jsonLen;
    char* json_buffer = readFile(argv[1],&jsonLen);
    size_t cbor_length;
    struct cbor_query* query;
    struct cbor_query* query2;
    
    printf("%s %s %s",argv[1],argv[2],argv[3]);

    if(strcmp(argv[3],"-e")==0){
        size_t contextMapLen;
	//Call the readFile function to read the context map file
        char* contextMap = readFile("../dictionaries/contextMap-ssn-examples-cdict.json",&contextMapLen);
        char* cborOut = encode_to_cbor_compressed(json_buffer,&cbor_length,panic,documentLoader,contextMap);
        //char* cborOut = encode_to_cbor_uncompressed(json_buffer,&cbor_length);
        char* outputFile = argv[2];
        printf("\nMain.c: OUTPUT: %s|\nSIZE: %ld\n",cborOut,strlen(cborOut));
        writeFileBinary(outputFile,cborOut,cbor_length);
    }
    if(strcmp(argv[3],"-d")==0){
        printf("\nMain.c: NOW DECODING... \n");
        char* cborIn = readFile(argv[2],&cbor_length);
        size_t contextMapLen;
        char* contextMap = readFile("../dictionaries/contextMap-ssn-examples-cdict.json",&contextMapLen);
        cJSON* json = decompress_decode_to_json(cborIn,&cbor_length,panic,documentLoader,contextMap);
        char* jsonString = cJSON_Print(json);
        printf("\n Main.c: FINAL OUTPUT:%s \n",jsonString);
        writeFile("output.jsonld",jsonString);
    }
    if(strcmp(argv[3],"-p")==0){
        /* Pretty parse the cborld data for debugging*/
        char* cborIn = readFile(argv[2],&cbor_length);
        /* `buffer` contains `cbor_length` bytes of input data */
        struct cbor_load_result result;
        cbor_item_t* item = cbor_load(cborIn, cbor_length, &result);
        free(cborIn);

        if (result.error.code != CBOR_ERR_NONE) {
            printf(
                "There was an error while reading the input near byte %zu (read %zu "
                "bytes in total): ",
            result.error.position, result.read);
            switch (result.error.code) {
                case CBOR_ERR_MALFORMATED: {
                    printf("Malformed data\n");
                    break;
                }
                case CBOR_ERR_MEMERROR: {
                    printf("Memory error -- perhaps the input is too large?\n");
                    break;
                }
                case CBOR_ERR_NODATA: {
                    printf("The input is empty\n");
                    break;
                }
                case CBOR_ERR_NOTENOUGHDATA: {
                    printf("Data seem to be missing -- is the input complete?\n");
                    break;
                }
                case CBOR_ERR_SYNTAXERROR: {
                    printf(
                    "Syntactically malformed data -- see "
                    "http://tools.ietf.org/html/rfc7049\n");
                    break;
                }
                case CBOR_ERR_NONE: {
                    // GCC's cheap dataflow analysis gag
                    break;
                }
            }
            exit(1);
        }

        /* Pretty-print the result */
        cbor_describe(item, stdout);
        fflush(stdout);
        /* Deallocate the result */
        cbor_decref(&item);
    }
    if(strcmp(argv[3],"-s")==0){
        /* Search for patterns inside the cborld data */
        char* cborIn = readFile(argv[2],&cbor_length);
        /* `buffer` contains `cbor_length` bytes of input data */
        LinkedList* query_list = createLinkedList();

        query = (struct cbor_query*) malloc(sizeof(struct cbor_query));
        query->s = NULL;
        query->p = NULL;
        query->o = NULL;
        
        //query->p = cbor_build_uint8(116);
        //query->o = cbor_build_string("JOHN");

        query->match_list = createLinkedList(); 
        pushLinkedList(query_list, (void*)query);
        
        query2 = (struct cbor_query*) malloc(sizeof(struct cbor_query));
        query2->s = NULL;
        //query2->p = NULL;
        //query->o = NULL;
        query2->o = NULL; 
        query2->p = cbor_build_uint8(200);
        query2->match_list = createLinkedList();
        pushLinkedList(query_list, (void*)query2);
        //XXX how to manage duplicate matches in separate queries

        struct cbor_load_result result;
        cbor_item_t* item = cbor_load(cborIn, cbor_length, &result);
        free(cborIn);
        //get tag and next move to map
        item = cbor_move(cbor_tag_item(item));
        get_all_matches_in_map(query_list, item, 0);//112 was encoded as id

        //Example of getting resuts
        printf("\n Example of getting results\n");
        print_query_results(query_list);

        if (result.error.code != CBOR_ERR_NONE) {
            printf(
                "There was an error while reading the input near byte %zu (read %zu "
                "bytes in total): ",
            result.error.position, result.read);
            switch (result.error.code) {
                case CBOR_ERR_MALFORMATED: {
                    printf("Malformed data\n");
                    break;
                }
                case CBOR_ERR_MEMERROR: {
                    printf("Memory error -- perhaps the input is too large?\n");
                    break;
                }
                case CBOR_ERR_NODATA: {
                    printf("The input is empty\n");
                    break;
                }
                case CBOR_ERR_NOTENOUGHDATA: {
                    printf("Data seem to be missing -- is the input complete?\n");
                    break;
                }
                case CBOR_ERR_SYNTAXERROR: {
                    printf(
                    "Syntactically malformed data -- see "
                    "http://tools.ietf.org/html/rfc7049\n");
                    break;
                }
                case CBOR_ERR_NONE: {
                    // GCC's cheap dataflow analysis gag
                    break;
                }
            }
            exit(1);
        }

        cbor_decref(&item);
    }

    if(strcmp(argv[3],"-mtp")==0){
        printf("\nMain.c: MATCHING TRIPLETS... \n");
        char* cborIn = readFile(argv[2],&cbor_length);
        // String subject = {
        //     .value = NULL,
        //     .size = 0
        // };
        //Here put the value of the subject string to be matched
        char* value = "urn:System1.LogicalView:Logical.BACnet.B.Block.Grouping.OccGrp";
        String subject = {
            .value = value,
            .size = strlen(value)
        };
        String object = {
            .value = NULL,
            .size = 0
        };
        // char* ovalue = "Occupancy grouping";
        // String object = {
        //     .value = ovalue,
        //     .size = strlen(ovalue)
        // };
        Triple pattern  = {
            .subject = &subject,
            .predicate = -1,
            .object = &object,
        };
        for (int i = 0; i < cbor_length; i++)
        {
            printf("%d ",(uint8_t)cborIn[i]);
        }
        printf("\n");
        size_t len=0;
        char* triples;
        match_tp(cborIn,cbor_length,pattern,&triples,&len);
        printf("\n\n%s \n %d\n",triples,len);
	writeFileBinary("./tmp.out",triples,len);
        // printf("\n");
        // for (int i = 0; i < len; i++)
        // {
        //     printf(" %c",(uint8_t)triples[i]);
        // }
        // printf("\n");
    }
    return 0;
}

