import rdflib
from collections import OrderedDict
import sys
import argparse

def main():
    # Set up argument parsing using argparse
    parser = argparse.ArgumentParser(description="Process RDF .ttl file with a given prefix")
    parser.add_argument("input_file", help="Path to the input RDF .ttl file")
    parser.add_argument("-p", "--prefix", help="Prefix to filter terms by")
    args = parser.parse_args()

    rdf_file = args.input_file
    prefix = args.prefix if args.prefix else "your_default_prefix:"
    

    # Initialize an RDF graph using the Turtle parser
    graph = rdflib.Graph().parse(rdf_file, format="ttl")

    # Create a defaultdict to store term counts
    result = []
    
    # Loop through each triple in the RDF graph
    for s, p, o in graph.triples((None, None, None)):
        subject_str = str(s).replace("[]", "").strip()
        object_str = str(o).replace("[]", "").strip()

        if (subject_str.startswith(prefix) and subject_str not in result):
            # Otherwise, add a new entry to the list with its initial count
            result.append((subject_str, 0))

    # Convert the list into an OrderedDict
    term_counts = OrderedDict(sorted(result))
    term_list = list(term_counts.items())

    tmp_count = 0
    for term, count in term_list:
        tmp_count += 1
        print("{}: {}".format(term, tmp_count))

if __name__ == "__main__":
    main()

