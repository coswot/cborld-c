# cborld-c

This is a library to compress jsonld documents to compressed cborld format. It is based on the specifications: https://digitalbazaar.github.io/cbor-ld-spec/

Note that different types of outputs may be interesting to different modules. For example some modules may be only interested in "terms" converted to integers and other modules would like to have these terms encoded/compacted into cbor for transmission. Note that it is not possible to compress all terms.

# API

```
char* context, char* contextMap

```

For converting jsonld to cborld and inverse, first one needs to load the contextMap and provide a callback function for loading context. An example is provided in `main.c`. In that it can be seen that the call to the following function  char* contextMap = readFile("../contextMap.json",&contextMapLen) will load the contextMap and the callback function documentLoader will load the context.

```
char* cborOut = encode_to_cbor_compressed(json_buffer,&cbor_length,panic,documentLoader,contextMap);
```

This function found in `encode.c` converts json-ld data to cbor-ld data. This needs a documentLoader callback to load context and needs contextMap. It returns cborld data as char*. An example is provided in `main.c`. The parameters are:

- json_buffer jsonld data

- cbor_length this variable will be used to return the length of cborld data

- panic function to be called in case of error

- documentLoader callback to load context

- contextMap containing context map 

```
cJSON* json = decompress_decode_to_json(cborIn,&cbor_length,panic,documentLoader,contextMap);
```

This function found in `decode.c` converts cbor-ld data to json object. It returns json object in cJSON format for which please see cJSON.h and .c. An example is provided in `main.c`. It takes the following parameters

- cborIn cborld data input

- cbor_length length of the cborld data

- panic function to be called in case of error

- documentLoader callback to load context

- contextMap containing context map 


```
cbor_item_t* get_all_matches_in_map(struct cbor_query* query, cbor_item_t* item, int encoded_id_int)
```

This function searches for triples that are present in cborld data where item points to the first 
cbor item in the data. The function then matches the pattern found in query and prints 
the matching triples. Note that this function requires that the item given to it should be 
a cbor MAP. Thus, tags etc. should be removed before passing the data.  
An example is provided in `main.c` where running the program with the option -s launches the search and prints the triples that are found.
The parameters are:

- query which indicates the pattern to search. The terms s,p,o in the query are cbor items. NULL means wildcard.

- item the pointer pointing to the first item which should be a map

- encoded_id_int the @id field is encoded as 4 by default, but if the value changes then it should be passed here. As it will be used to locate the subject.

# 1 - Install dependencies
This cborld library mostly requires libcbor and cmake.

These steps have been tested on Debian 10 and 11 (cmake versions 3.13 and 3.18): 
```
sudo apt-get install libcbor-dev

sudo apt-get install cmake
```
optionally you may also install hexedit to view raw output in hexa:

```
sudo apt-get install hexedit
```
You may read about libcbor here - https://libcbor.readthedocs.io/en/latest/getting_started.html

# 2 - Compilation
Download the respository and cd to the main directory of the repository. 

Then to compile it please follow these steps:

```
mkdir build
cd build
cmake ..
cmake --build .
```

# 3 - Demo
Lets compress and decompress a jsonld file (present in data folder) to cborld. 

## Compression
Please check main.c file which provides an example of how to use this cborld library. Note that to compress different  jsonld files, one needs to load the appropriate context inside the function char* documentLoader(char* url).

From the file main.c it can be seen that -e is for compressing, -d for decompressing and -mtp for matching triples.

```
./CBORLD ../data/ssn-example-1.jsonld out1.cborld -e
```
This will run the example main.c file and compress prc.jsonld to out.cborld

The contents of the file out.cborld can be viewed using hexedit.

For debugging purposes, currently we output lots of text. 


## Decompression

You may decode the above out.cborld file using the option -d

```
./CBORLD ../data/ssn-example-1.jsonld out1.cborld -d
```

Note that in the current example main.c, we still give a valid file name as argument 1 even if it is not needed.

# Search Triples

The triples can be found using the option -s

This is an example based on prc.jsonld file. In th example we have query->s = NULL; query->p = NULL; query->o = NULL; where
NULL means wildcard. Thus, all triples will be printed (except the blank nodes as we cannot look for them at present because they do not have @id in this json-ld format.

One may try other examples as well: query->o = cbor_build_string("JOHN"); will look for triple where o is "JOHN".
Wildcard query will print the following. Array 2 issuer... means https://issuer... as triples are still encoded to save space.

```
 array:2 issuer.oidp.uscis.gov/credentials/83627465 116 array:108 178 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 138 did:example:28394728934792387 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 184 83627465 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 188 Permanent Resident Card 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 182 Government of Example Permanent Resident Card. 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 134 1575368392 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 130 1890987592 
 did:example:b34ca6cd37bbf238 116 array:176 180 
 did:example:b34ca6cd37bbf238 202 JOHN 
 did:example:b34ca6cd37bbf238 198 SMITH 
 did:example:b34ca6cd37bbf238 200 Male 
 did:example:b34ca6cd37bbf238 186 data:image/png;base64,iVBORw0KGgo...kJggg== 
 did:example:b34ca6cd37bbf238 208 1420107592 
 did:example:b34ca6cd37bbf238 204 C09 
 did:example:b34ca6cd37bbf238 206 999-999-999 
 did:example:b34ca6cd37bbf238 194 C1 
 did:example:b34ca6cd37bbf238 190 Bahamas 
 did:example:b34ca6cd37bbf238 192 -361629608 
 array:2 issuer.oidp.uscis.gov/credentials/83627465 126 did:example:b34ca6cd37bbf238

```
